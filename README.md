# Generic File-Icons

Here's some very simple and neutral file-icons icons that can be used for
pretty much whatever you want.

I've created these for various minor uses, and if they can be of use to
you, all the better! :smile:

This is a very small collection, but I expect to extend it a bit as time
goes on. If you've made some variations that you'd like included, please
send a merge-request!

## Design

The icons are horizontally centered on a 16x16 grid, mostly using integer
coordinates for clean rendering on low-density displays. The files
themselves take up 12 x 16, approximating the aspect ratio of the
[A-series][1] paper size, while both dimensions are even so they center
cleanly.

Apart from that, most details are as simple as possible to keep things
small and obvious for the user. The icons work best on very bright or
dark backgrounds, because there's no borders or shadows. You can add
drop shadows using HTML if needed to make them stand out more.

The SVG is hand-written for optimal results. But of course, even better
results can be archieved by using an SVG-optimizer (like [svgo][2]) on top.

## Icons

| Filename      | Meaning       | Image                            |
|---------------|---------------|----------------------------------|
| `file.svg`    | Basic file    | ![File Icon](svg/file.svg)       |
| `archive.svg` | zip / tarball | ![Archive Icon](svg/archive.svg) |
| `sig.svg`     | PGP Signature | ![Signature Icon](svg/sig.svg)   |
| `exe.svg`     | Executable    | ![Executable Icon](svg/exe.svg)  |
| `info.svg`    | Info file     | ![Info Icon](svg/info.svg)       |
| `folder.svg`  | Folder        | ![Folder Icon](svg/folder.svg)   |
| `parent.svg`  | Parent folder | ![Parent Icon](svg/parent.svg)   |

## License

These icons are licensed under the [MIT license][3]. See
[`LICENSE.md`](LICENSE.md) for details.

Contact me if you want to use them under other license.

[1]: https://en.wikipedia.org/wiki/Paper_size#A_series
[2]: https://github.com/svg/svgo
[3]: https://choosealicense.com/licenses/mit/
